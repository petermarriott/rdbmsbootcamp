-- Set-up some test data
truncate table reporting.barcodes_focus;
insert into reporting.barcodes_focus (barcode, category_id, first_sale_date)
values ('7312040017034', 102, '2012-12-06'),
       ('7312040017201', 102, '2009-05-30'),
       ('7312040017355', 102, '2012-03-10'),
       ('7312040040704', 102, '2009-06-23'),
       ('7312040090709', 102, '2009-05-02'),
       ('7312040322503', 102, '2017-07-24'),
       ('7312040323043', 102, '2022-09-16'),
       ('7312040350056', 102, '2022-04-02'),
       ('7312040350100', 102, '2022-03-27'),
       ('7312040552528', 102, '2020-03-06'),
       ('9999999999999', 102, current_date);

-- coalesce
select *
     , coalesce(dob, '1950-01-01')
FROM reporting.client;

select *
from reporting.client
where coalesce(dob, '1950-01-01') >= '2000-01-01';

select coalesce(postcode2, dob::varchar, client.client)
     , *
from reporting.client
;

-- case statement -- simple
select case clientid / 1000 when 5 then 'A list' else 'the rest' end as status
     , clientid / 1000                                               as working
     , *
from reporting.client
order by status;

truncate table reporting.barcodes_focus;
insert into reporting.barcodes_focus (barcode, category_id, first_sale_date)
values ('7312040017034', 102, '2012-12-06'),
       ('7312040017201', 102, '2009-05-30'),
       ('7312040017355', 102, '2012-03-10'),
       ('7312040040704', 102, '2009-06-23'),
       ('7312040090709', 102, '2009-05-02'),
       ('7312040322503', 102, '2017-07-24'),
       ('7312040323043', 102, '2022-09-16'),
       ('7312040350056', 102, '2022-04-02'),
       ('7312040350100', 102, '2022-03-27'),
       ('7312040552528', 102, '2020-03-06'),
       ('9999999999999', 102, current_date);

-- with ands
select case
           when category_id = 102 and first_sale_date < '2017-01-01' then 'Old'
           when category_id = 102 and first_sale_date < '2022-01-01' then 'Established'
           when category_id = 102 and first_sale_date < '2023-01-01' then 'Recent'
           else 'MPD'
    end as Status
     , *
from reporting.barcodes_focus;

-- using a case statement group bys
select count(case when category_id = 102 and first_sale_date < '2017-01-01' then 1 else null end)                        as old_count
     , count(case when category_id = 102 and first_sale_date between '2017-01-01' and '2021-12-31' then 1 else null end) as established_count
     , count(case when category_id = 102 and first_sale_date between '2022-01-01' and '2023-12-31' then 1 else null end) as recent_count
     , count(case when category_id = 102 and first_sale_date >= '2023-01-01' then 1 else null end)                       as mpd_count
from reporting.barcodes_focus;

