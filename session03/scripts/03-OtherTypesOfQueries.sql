
------------------------------
-- Subqueries (or Sub selects)
------------------------------

-- A basic sub select
select * 
from reporting.detail1
where themasterid not in
(select themasterid
   from reporting.detail2);

-- A sub select with a group by
select * from reporting.folder_list
where fileondisk
in (select fileondisk
   from reporting.folder_list
group by fileondisk
having count(fileondisk) > 1)
order by 4, 2;

-- Correlated subquery - very bad for performance

select p1.*
from reporting.folder_list p1 
where p1.FileOnDisk in
(select p2.FileOnDisk
   from reporting.folder_list p2
  where p1.MachineId = p2.MachineId 
    and p1.FileOnDisk = p2.FileOnDisk
    and p1.FilePath <> p2.FilePath
);

-- Better way

select p1.*
from reporting.folder_list p1
left outer join reporting.folder_list p2 on p1.FileOnDisk = p2.FileOnDisk
                                                and p1.MachineId = p2.MachineId
                                                and p1.FileOnDisk = p2.FileOnDisk
                                                and p1.FilePath <> p2.FilePath
where p2.id is not null;

-- Don't take my word for it

explain analyze verbose
select p1.*
from reporting.folder_list p1
where p1.FileOnDisk in
(select p2.FileOnDisk
   from reporting.folder_list p2
  where p1.MachineId = p2.MachineId
    and p1.FileOnDisk = p2.FileOnDisk
    and p1.FilePath <> p2.FilePath
);

-- Better way

explain analyze verbose
select p1.*
from reporting.folder_list p1
left outer join reporting.folder_list p2 on p1.FileOnDisk = p2.FileOnDisk
                                                and p1.MachineId = p2.MachineId
                                                and p1.FileOnDisk = p2.FileOnDisk
                                                and p1.FilePath <> p2.FilePath
where p2.id is not null;

---------------------------
-- Common Table Expressions
---------------------------

with MyCTE as (
	select clientid, appointdate
	  from reporting.appointment
    where appointdate between '2008-01-01' and '2009-01-01'
)
select c.*, m.*
from MyCTE m
inner join reporting.client c on c.clientid = m.clientid
order by 1, 3;

select c.client, m.*
from 
(
	select clientid, appointdate
	  from reporting.appointment
    where appointdate between '2008-01-01' and '2009-01-01'
) m
inner join reporting.client c on c.clientid = m.clientid
order by 1, 3;

-- Practical 2

-- 1. Get all the CLIENTS that don't have a record in the CLIENTCONTRACT table

-- 2. Get all the CLIENTS that didn't have an appointment in 2008

-- 3. Use a CTE to get the client details and Appointment date of the last 20 Appointments
