
drop table if exists list1;
drop table if exists list2;
create temporary table list1 (id int, data varchar(30));
create temporary table list2 (id int, data varchar(20));
insert into list1 (id, data) select generate_series(1, 200, 2), reporting.random_string(cast(random() * 26 as int) +4);
insert into list2 (id, data) select generate_series(1, 300, 3), reporting.random_string(cast(random() * 16 as int) +4);

select * from list1;
select * from list2;


-- Inner join
-------------

select l1.*, l2.*
from list1 l1
inner join list2 l2
on l1.id = l2.id;

-- Left Outer Join
------------------

select l1.*, l2.*
from list1 l1
left outer join list2 l2
on l1.id = l2.id;

-- Full outer Join
------------------

select l1.*, l2.*
from list1 l1
full outer join list2 l2
on l1.id = l2.id;

-- Cross Join
--------------------
select l1.*, l2.*
from list1 l1
cross join list2 l2;

-- Cartesian product (other name for Cross join)
--------------------
select distinct l1.*, l2.*
from list1 l1, list2 l2;

----------------------------
-- Search criteria placement 
----------------------------

-- Inner join
select l1.*, l2.*
from list1 l1
inner join list2 l2
on l1.id = l2.id 
where l2.data like '%s%';

-- Same except outer join
select l1.*, l2.*
from list1 l1
left outer join list2 l2
on l1.id = l2.id 
where l2.data like '%s%';
-- same number of rows again

-- Left Outer Join criteria placement
select l1.*, l2.*
from list1 l1
left outer join list2 l2
on l1.id = l2.id and l2.data like '%s%';
-- 100 rows (only 15 from list2)

-----------------
-- Join into self
-----------------

select d1.* , d2.detail1data
from reporting.Detail1 d1
inner join reporting.Detail1 d2 on length(D1.DETAIL1DATA) = length(D2.DETAIL1DATA)
where D1.DETAIL1ID != D2.DETAIL1ID
--and length(D1.DETAIL1DATA) = 2
order by d1.detail1data, d2.detail1data;

--------------------
-- Union & Union All
--------------------

select m.*
from reporting.the_master m
 inner join reporting.detail1 d1 on m.themasterid = d1.themasterid
union
select m.*
from reporting.the_master m
 inner join reporting.detail2 d2 on m.themasterid = d2.themasterid
order by 1;

select m.*
from reporting.the_master m
 inner join reporting.detail1 d1 on m.themasterid = d1.themasterid
union all
select m.*
from reporting.the_master m
 inner join reporting.detail2 d2 on m.themasterid = d2.themasterid
order by 1;

-- Practicals 1

select * from reporting.client;
select * from reporting.appointment;

-- 1. Inner Join the client table, to the appointment table on the clientid column.
--    only get clients with the Postcode1 of CB1

-- 2. Now change the select to a left outer join, how has the result set changed?

-- 3. Now change the select so you get all rows back from the Client table but still
--    only get appointments information from the Client table where the post code is CB1
