-- Search criteria

-- Simple = and not equals

SELECT *
  FROM reporting.client
  where surname = 'Alexander'
  and firstname <> 'Christopher';

-- Greater than 

SELECT *
  FROM reporting.client
where dob > '1969-01-01';

-- Greater than and equals

SELECT *
  FROM reporting.client
where dob >= '1969-01-01';

-- Nulls!!

SELECT *
  FROM reporting.client
  where postcode1 is null;

SELECT *
  FROM reporting.client
  where postcode1 = '';
  
-- Note not nulls found

SELECT *
  FROM reporting.client
  where postcode1 is null or postcode1 = '';

-- Cope with single quotes

SELECT *
  FROM reporting.client
  where surname = 'O''Sullivan';

-- Like - anywhere

SELECT *
  FROM reporting.client
  where client like '%00%';
  
-- Like - starts with
SELECT *
  FROM reporting.client
  where client like 'CA00%';

-- Like - ends with
SELECT *
  FROM reporting.client
  where client like '%0';

-- Like - signal wild card character

SELECT *
  FROM reporting.client
  where postcode1 like 'CB_';

-- Other pattern matching

DROP TABLE if exists PatternMatching;

create table PatternMatching (column1 varchar(30));


DROP TABLE if exists PatternMatching;

create table PatternMatching (column1 varchar(30));

INSERT INTO PatternMatching (column1) VALUES ('Test 1234');
INSERT INTO PatternMatching (column1) VALUES ('Test_1234');
INSERT INTO PatternMatching (column1) VALUES ('Test%1234');
INSERT INTO PatternMatching (column1) VALUES ('aaaaa');
INSERT INTO PatternMatching (column1) VALUES ('AAAAA');
INSERT INTO PatternMatching (column1) VALUES ('bbbbb');
INSERT INTO PatternMatching (column1) VALUES ('ccccc');
INSERT INTO PatternMatching (column1) VALUES ('ddddd');
INSERT INTO PatternMatching (column1) VALUES ('eeeee');
INSERT INTO PatternMatching (column1) VALUES ('abcde');
INSERT INTO PatternMatching (column1) VALUES ('acde');
INSERT INTO PatternMatching (column1) VALUES ('aabcde');
INSERT INTO PatternMatching (column1) VALUES ('aabb');

SELECT * FROM PatternMatching where column1 like 'Test_1234';
SELECT * FROM PatternMatching where column1 like 'Test%1234';
-- Like - catching a real underscore
SELECT * FROM PatternMatching where column1 like 'Test\_1234' escape '\';
-- Like - catching a real %
SELECT * FROM PatternMatching where column1 like 'Test\%1234' escape '\';

-- Sets
SELECT * FROM PatternMatching where column1 like '%a%';
SELECT * FROM PatternMatching where column1 like '%aa%';
SELECT * FROM PatternMatching where column1 like '%ab%';
SELECT * FROM PatternMatching where column1 like '___abc%';

-- being negative
SELECT * FROM PatternMatching where column1 not like '__cde%';

-- Regular expressions

SELECT * FROM PatternMatching where column1 similar to 'aaaaa';
SELECT * FROM PatternMatching where column1 similar to '(a|b)%';
SELECT * FROM PatternMatching where column1 similar to '(a|b)*'; -- All a's or all b's
SELECT * FROM PatternMatching where column1 similar to 'a(a|b|c)%';

-- in and not in

SELECT *
  FROM reporting.client
  where firstname in ('Christopher','Muhammad','Sebastian');

SELECT *
  FROM reporting.client
  where firstname not in ('Christopher','Muhammad','Sebastian');

-- Between

SELECT *
  FROM reporting.client
  where dob between '1960-01-01' and '1970-01-07'
order by dob;

-- Note both dates included

-- Practical 01

-- 1.	In reporting.client find all the people with 'Christopher' as their firstname apart from those
-- with 'Alexander' as their surname

-- 2.	In reporting.client find all the people with dob between '1970-01-01' and '2000-01-01' and there postcode1 is 'PE13'

-- 3.	In reporting.client find all the people who have a postcode1 starting with 'CB'

-- 4.	In reporting.client find all the people who don't have a postcode1 starting with 'PE' remember nulls

-- 5.	In reporting.client find all the people who have postcode1 of 'PE15', 'PE19' or 'CB4' using in

-- 6.	In reporting.client find all the people who have postcode1 of 'PE2' or 'PE6' using similar to 

|