-- Populate data

-- Sales data 5,000,000 rows

truncate table reporting.sales;
insert into reporting.sales
    (id, barcode, shop_id, qty, sold_at)
select generate_series(1, 5000000),
       reporting.random_barcode_1000(true),
       reporting.random_number(4),
       reporting.random_number(1),
       CURRENT_TIMESTAMP - reporting.random_number(4) * interval '1 hour';

-- barcodes data 1,000,000 rows

truncate table reporting.barcodes;
insert into reporting.barcodes
    (barcode, product_description, category_id)
select barcodes.barcode, reporting.random_string(cast(random() * 70 as int) + 20), reporting.random_number(3)
from (
         select distinct barcode
         from (
                  select reporting.random_barcode(true) as barcode
                  from generate_series(1, 1000000)) as ran_barcode) as barcodes;

-- Clear out any that clash with sales barcodes to avoid duplicates

delete
from reporting.barcodes
where barcode in (select barcode from reporting.sales);

-- Add in sales barcodes

insert into reporting.barcodes
    (barcode, product_description, category_id)
select barcode, reporting.random_string(cast(random() * 70 as int) + 20), reporting.random_number(2)
from (select distinct barcode
      from reporting.sales) as barcodes;

-- Create focused sales set of barcodes

truncate table reporting.barcodes_focus;
insert into reporting.barcodes_focus
    (barcode, category_id, first_sale_date)
select first_sale_date.barcode,
       b.category_id,
       first_sale_date.first_sale_date
from (
         select barcode, cast(min(sold_at) as date) as first_sale_date
         from reporting.sales
         group by barcode) as first_sale_date
         inner join reporting.barcodes b on b.barcode = first_sale_date.barcode
where b.category_id = (select category_id
                       from (
                                select category_id,
                                       count(category_id)
                                from reporting.barcodes b
                                where b.barcode in (select barcode from reporting.sales)
                                group by category_id
                                order by 2 desc) as top_cat
                       limit 1)
;

delete from reporting.barcodes_focus
where barcode in (
select barcode
from (
select barcode, ROW_NUMBER() over (order by Barcode asc) as RowNumber
 from reporting.barcodes_focus) as b_rows
 where RowNumber > (select floor(count(*)/2) from reporting.barcodes_focus))
;

-- Tables all have a primary key

-- Straight to Practical

-- Practical 5

-- For each question drop the indexes from the question before to get a true picture

-- 1. What index do you want to add to the sales table to efficiently find the products sold on a single day

set search_path = reporting;

--explain analyse
select *
from reporting.sales where sold_at between '2020-07-01' and '2020-07-02'
order by shop_id,sold_at,barcode;

create index idx_sold on reporting.sales (sold_at);
drop index if exists reporting.idx_sold;
drop index if exists reporting.idx_category_id;
drop index if exists reporting.idx_barcode;
-- 2. What index do you want to add to the sales and or barcode table to easily find the best selling category_id of last month

explain analyse
select b.category_id, sum(s.qty) as sum_qty
from reporting.sales s
inner join reporting.barcodes b
on s.barcode = b.barcode
where s.sold_at between '2020-07-01' and '2020-07-11'
and s.barcode like '%9'
group by b.category_id
order by 2 desc
--and b.category_id = 200
limit 10;

create index idx_sold on reporting.sales (sold_at);
create index idx_category_id on reporting.barcodes (category_id);
create index idx_barcode on reporting.sales (barcode);

-- 3. What index do you want to add to the sales and or barcode table to easily find the best selling category_id of last month

explain analyse
select b.category_id, sum(s.qty) as sum_qty from reporting.sales s
inner join reporting.barcodes b
on s.barcode = b.barcode
where s.sold_at between '2020-07-01' and '2020-08-01'
group by b.category_id
order by sum_qty desc, b.category_id
limit 10;

-- x. What index do you want to add to the barcode table to easily find the focused barcodes
