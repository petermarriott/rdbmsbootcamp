--------------------
-- Ranking Functions
--------------------

-------------
-- Row Number
-------------

-- You can number your rows

select ROW_NUMBER() over (order by MachineId, FileOnDisk) as RowNumber, 
    MachineId, id, FILEONDISK
  from reporting.folder_list
 order by MachineId, FileOnDisk;

-- Regardless of the order of the results set

select ROW_NUMBER() over (order by MachineId, FileOnDisk) as RowNumber, 
    MachineId, id, FILEONDISK
  from reporting.folder_list
 order by MachineId, id;

-- Example
-- Use a CTE to get the appointments of the 3 Clients with Surnames with 
-- the highest alphabetic order

-- First number your rows
select ROW_NUMBER() over (order by SURNAME asc) as RowNumber, CLIENTID, SURNAME, FIRSTNAME
	from reporting.client
 order by SURNAME asc;

-- Use the row number in a CTE
select *
  from reporting.appointment a
  inner join (
select ROW_NUMBER() over (order by SURNAME asc) as RowNumber, CLIENTID, SURNAME, FIRSTNAME
	from reporting.client
  ) cl on a.clientid = cl.CLIENTID and cl.RowNumber < 4
order by A.APPOINTDATE;

--------------------
-- Rank & Dense Rank
--------------------

-- Rank

 select RANK() over (order by FileOnDisk) as RowRank, 
    MachineId, FILEONDISK
  from reporting.folder_list
 order by RowRank;
 
-- Dense Rank
 
 select DENSE_RANK() over (order by FileOnDisk) as DenseRowRank, 
    MachineId, FILEONDISK
  from reporting.folder_list
 order by DenseRowRank;
 
-------------
-- Partioning 
-------------

-- Partioning with RANK

 select RANK() over (PARTITION BY MachineId,FileOnDisk order by FilePath) as DupOnSameMachine, 
    MachineId, FILEONDISK
  from reporting.folder_list
 order by FileOnDisk;

-- With a Common table expression (CTE)

with MyRankingTable as
(
 select RANK() over (PARTITION BY MachineId,FileOnDisk order by FilePath) as RowRankPart, 
    MachineId, FILEONDISK
  from reporting.folder_list
)
  select * from MyRankingTable
  where RowRankPart = 2
 order by FileOnDisk;

-- Partioning with RANK and ROW_NUMBER to chose between equals

 select RANK() over (PARTITION BY MachineId,FileOnDisk order by FilePath) as DupOnSameMachine, 
		ROW_NUMBER() over (PARTITION BY FileOnDisk order by MachineId,FilePath) as DupOnAnyMachine2, 
    MachineId, FILEONDISK
  from reporting.folder_list
 order by FileOnDisk;

-- Ntile

drop table if exists list_ntile;
create temporary table list_ntile (id int, data int, bigdata numeric);
insert into list_ntile (id, data, bigdata) select generate_series(1, 200, 2), cast(random() * 26 as int), exp(random() * 200);

select * from list_ntile;

select id, data, NTILE(10) over (order by data)
from list_ntile;

select id, data, NTILE(10) over (order by data)
from list_ntile
where data > 10;


-- Practical 3

-- 1.  Number the rows in DETAIL1 by the DETAIL1DATA column descending

-- 2.  Now partition this by THEMASTERID

-- 3.  Putting the answer from 2 into a CTE get all the rows with a row number of 2
