
-- Basic function returning a value

Create or replace function reporting.random_int(max_value integer) returns int as
$$
declare
  result int := 0;
begin
  if max_value < 1 then
    raise exception 'Given length cannot be less than 0';
  end if;
  result := ceil(random() * max_value);
  return result;
end;
$$ language plpgsql;

select generate_series(1, 20, 1), reporting.random_int(50);

-- Output parameters
--drop function reporting.add_vat;
Create or replace function reporting.add_vat(subtotal real, rate real, OUT net real, OUT vat real) AS $$
BEGIN
    vat := subtotal * rate;
    net := subtotal - vat;
END;
$$ LANGUAGE plpgsql;

select * from reporting.add_vat(2.99, 0.2);

select reporting.add_vat(clientid, 0.2) from reporting.client limit 10;

-- Table results
--drop function reporting.top_counters;
CREATE OR REPLACE FUNCTION reporting.top_counters(get_RowRankPart int)
RETURNS TABLE (got_RowRankPart bigint, got_machineid int, got_fileondisk varchar(40)) AS $$
begin
    return query
  with MyRankingTable as
(
 select RANK() over (PARTITION BY MachineId,FileOnDisk order by FilePath) as RowRankPart,
    MachineId, FILEONDISK
  from reporting.folder_list
)
  select RowRankPart, machineid, fileondisk from MyRankingTable
  where RowRankPart = get_RowRankPart
 order by FileOnDisk;
  end;
$$ LANGUAGE plpgsql;

select * from reporting.top_counters(2);

-- Practical 4

-- Free style