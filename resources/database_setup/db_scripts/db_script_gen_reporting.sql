DO LANGUAGE plpgsql $$
    BEGIN
        IF (select current_database()) not like '%prod%' THEN
          drop schema if exists reporting cascade;
          create schema reporting AUTHORIZATION "app_owner";
        ELSE
          RAISE EXCEPTION 'Wrong database';
        END IF;
    END;
$$;

SET search_path = reporting;

Create or replace function reporting.random_string(length integer) returns text as
$$
declare
  chars text[] := '{0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z}';
  result text := '';
  i integer := 0;
begin
  if length < 0 then
    raise exception 'Given length cannot be less than 0';
  end if;
  for i in 1..length loop
    result := result || chars[1+random()*(array_length(chars, 1)-1)];
  end loop;
  return result;
end;
$$ language plpgsql;

SET search_path = reporting;

Create or replace function reporting.random_barcode(is_13 bool) returns text as
$$
declare
  chars text[] := '{0,1,2,3,4,5,6,7,8,9}';
  result text := '';
  length integer := 0;
  i integer := 0;
begin
  if is_13 = true then
    length := 13;
  else
    length := 8;
  end if;
  for i in 1..length loop
    result := result || chars[1+random()*(array_length(chars, 1)-1)];
  end loop;
  return result;
end;
$$ language plpgsql;

Create or replace function reporting.random_barcode_1000(is_13 bool) returns text as
$$
declare
  chars text[] := '{0,1,2,3,4,5,6,7,8,9}';
  result text := '';
  length integer := 0;
  i integer := 0;
begin
  if is_13 = true then
    result := '5302222345';
  else
    result := '54022';
  end if;
  length := 3;
  for i in 1..length loop
    result := result || chars[1+random()*(array_length(chars, 1)-1)];
  end loop;
  return result;
end;
$$ language plpgsql;

Create or replace function reporting.random_number(digits int) returns int as
$$
declare
  chars text[] := '{0,1,2,3,4,5,6,7,8,9}';
  result text := '';
  i integer := 0;
begin
  if digits < 0 then
    raise exception 'Given length cannot be less than 0';
  end if;
  for i in 1..digits loop
    result := result || chars[1+random()*(array_length(chars, 1)-1)];
  end loop;
  return cast(result as int);
end;
$$ language plpgsql;

SET search_path = reporting;

drop table if EXISTS reporting.the_master;
drop table if EXISTS reporting.detail1;
drop table if EXISTS reporting.detail2;

create table reporting.the_master (
	TheMasterID int NOT NULL,
	TheMasterData varchar(15) NOT NULL);

create table reporting.detail1 (
    Detail1ID int NOT NULL,
    TheMasterID int NOT NULL,
    Detail1Data varchar(15) NULL);

create table reporting.detail2 (
    Detail2ID int NOT NULL,
    TheMasterID int NOT NULL,
    Detail2Data varchar(15) NULL);

SET search_path = public;

drop table if exists reporting.Client;
drop table if exists reporting.ClientContract;
drop table if exists reporting.Appointment;

create table reporting.Client (
    ClientID int NOT NULL,
    Client varchar(15) NOT NULL,
    Surname varchar(50) NULL,
    Firstname varchar(50) NULL,
    Postcode1 varchar(4) NULL,
    Postcode2 varchar(3) NULL,
    DOB date NULL);

create table reporting.ClientContract (
    ContractID int NOT NULL,
    ClientID int NOT NULL,
    ReferralDate date NULL,
    EngagementDate date NULL,
    CompletionDate date NULL);

create table reporting.Appointment (
    AppointID int NOT NULL,
    ClientID int NOT NULL,
    AppointDate date NULL);    

drop table if exists reporting.folder_list;
drop sequence if exists reporting.folder_list_seq;

create table reporting.folder_list
(id int,
 MachineId  int,
 FilePath   varchar(128),
 FileOnDisk varchar(40));

create sequence reporting.folder_list_seq
owned by reporting.folder_list.id;

drop table if exists reporting.sales;
create table reporting.sales
(
    id int not null constraint sales_pkey
			primary key,
	barcode varchar(18) not null,
    shop_id int,
    qty int,
    sold_at timestamp
);

drop table if exists reporting.barcodes;
create table reporting.barcodes
(
	barcode varchar(18) not null constraint barcodes_pkey
			primary key,
    product_description varchar(255),
    category_id int
);

drop table if exists reporting.barcodes_focus;
create table reporting.barcodes_focus
(
    barcode varchar(18) not null constraint barcodes_focus_pkey
			primary key,
    category_id int,
    first_sale_date date
);

GRANT ALL ON SCHEMA reporting TO app_read_write;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA reporting TO app_read_write;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA reporting TO app_read_write;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA reporting TO app_read_write;
ALTER DEFAULT PRIVILEGES IN SCHEMA reporting GRANT ALL ON TABLES TO app_read_write;
ALTER DEFAULT PRIVILEGES IN SCHEMA reporting GRANT ALL ON SEQUENCES TO app_read_write;
ALTER DEFAULT PRIVILEGES IN SCHEMA reporting GRANT ALL ON FUNCTIONS TO app_read_write;

GRANT USAGE ON SCHEMA reporting TO app_read;
GRANT SELECT ON ALL TABLES IN SCHEMA reporting TO app_read;
ALTER DEFAULT PRIVILEGES IN SCHEMA reporting GRANT SELECT ON TABLES TO app_read;

GRANT USAGE ON SCHEMA reporting TO app_write;
GRANT INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA reporting TO app_write;
ALTER DEFAULT PRIVILEGES IN SCHEMA reporting GRANT INSERT, UPDATE, DELETE ON TABLES TO app_write;

