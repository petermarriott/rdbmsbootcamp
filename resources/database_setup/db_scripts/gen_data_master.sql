do $$
declare
  masterCount integer := 0;
  detail1Count integer := 0;
  detail2Count integer := 0;
  detailLoopCount integer;
begin
  truncate table reporting.the_master;
  truncate table reporting.detail1;
  truncate table reporting.detail2;
  while masterCount < 26
  loop
    masterCount := masterCount + 1;
    insert into reporting.the_master (TheMasterID, TheMasterData) values (masterCount,RPAD(CHR(64+masterCount),14,CHR(64+masterCount)));
    detailLoopCount := 0;
    while detailLoopCount < 3
    loop
      detailLoopCount := detailLoopCount + 1;
      if CHR(64+masterCount) in ( 'A', 'E', 'I', 'O', 'U') then
        detail1Count := detail1Count + 1;
        insert into reporting.detail1 (Detail1ID, TheMasterID, Detail1Data)
          values (detail1Count, masterCount, RPAD(CHR(96+masterCount) , detailLoopCount, CHR(96+masterCount)));
      elsif masterCount > 10 then
        detail2Count := detail2Count + 1;
        insert into reporting.detail2 (Detail2ID, TheMasterID, Detail2Data)
          values (detail2Count, masterCount, RPAD(CHR(96+masterCount) , detailLoopCount, CHR(96+masterCount)));
      else
        detail1Count := detail1Count + 1;
        insert into reporting.detail1 (Detail1ID, TheMasterID, Detail1Data)
          values (detail1Count, masterCount, RPAD(CHR(96+masterCount) , detailLoopCount, CHR(96+masterCount)));
        detail2Count := detail2Count + 1;
        insert into reporting.detail2 (Detail2ID, TheMasterID, Detail2Data)
          values (detail1Count, masterCount, RPAD(CHR(96+masterCount) , detailLoopCount, CHR(96+masterCount)));
      end if;
    end loop;
  end loop;
  delete from reporting.the_master where TheMasterID in (2,20);
  delete from reporting.detail1 where TheMasterID in (3,4);
  delete from reporting.detail2 where TheMasterID in (5,6);
  commit;
end; $$