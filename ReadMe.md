# Relational Database Management System - Bootcamp

## Set-up database

```bash
cd resources
docker-compose up --detach
docker-compose logs --follow --timestamps
```

Stopping container: `docker-compose stop`
Stopping deleting: `docker-compose rm`
Stopping rebuilding: `docker-compose build`

```bash
docker-compose stop
docker-compose rm
docker-compose build
```
