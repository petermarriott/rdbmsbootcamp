-------------
-- limit clause
-------------

SELECT *
  FROM reporting.client;

SELECT *
  FROM reporting.client
limit 20;

------------------
-- Order by clause
------------------

SELECT *
  FROM reporting.appointment
order by clientid, appointdate;

SELECT *
  FROM reporting.appointment
order by clientid, appointdate desc
limit 20;


-------------------
-- Distinct keyword
-------------------

SELECT firstname
  FROM reporting.client;

SELECT distinct firstname
  FROM reporting.client;

------------------
-- Order by clause
------------------

SELECT *
  FROM reporting.appointment
order by clientid, appointdate;

SELECT *
  FROM reporting.appointment
order by clientid, appointdate desc;

-------------------
-- String Functions
-------------------
-- https://www.postgresql.org/docs/11/functions-string.html
-- substring


select substring(surname,1,7) as FirstBit,
       substring(surname,4,7) as MiddleBit,
       left(surname,7) as LeftBit,
       right(surname,7) as RightBit,
       REPLACE(postcode1,'CB','WE') as MessedWith,
       *
 from  reporting.client;

select *
 from  reporting.client
 where REPLACE(postcode1,'CB','WE') = 'WE24';


-- Concatenate

select 'Name: ' || firstname || ' ' || surname
 from  reporting.client;

-- Be where -- we will fix this when we get to system functions
select 'Name: ' || firstname || ' ' || surname || postcode2
 from  reporting.client;

-- Lower and Upper case
-- Lower and Upper case
select lower(firstname),
        upper(firstname)
from  reporting.client;
-- charindex and len

select filepath,
       position('\' in filepath),
       length(filepath)
 from reporting.folder_list;

-- charindex and substring

select filepath
       ,       position('\' in filepath)
       ,       substring(filepath,1,position('\' in filepath) - 1)
       ,       substring(filepath,position('\' in filepath) + 1,length(filepath))
from reporting.folder_list
where position('\' in filepath) > 0;

--------------------------
-- Date and Time Functions 
--------------------------

-- Date Diff
-- Days
select completiondate - engagementdate as days_between
from reporting.clientcontract;

-- Date Add
select engagementdate,
       completiondate,
       completiondate + interval '1 minute',
       completiondate + interval '1 hour',
       completiondate + interval '1 day',
       completiondate + interval '1 month',
       completiondate + interval '1 year',
       completiondate + interval '1 year' * 3
from reporting.clientcontract
where completiondate - engagementdate > 35;

-- Get date
select current_date,
       current_time,
       current_timestamp;

-- Date Part
select current_timestamp,
       date_part('minute',current_timestamp),
       date_part('hour',current_timestamp),
       date_part('day',current_timestamp),
       date_part('month',current_timestamp),
       date_part('year',current_timestamp);
-- Date name
SELECT
  TO_CHAR(current_timestamp, 'month') AS "month",
  TO_CHAR(current_timestamp, 'Month') AS "Month",
  TO_CHAR(current_timestamp, 'MONTH') AS "MONTH";


-- Practical 1

-- 1. Get all Items from reporting.folder_list where the fileondisk starts with 'Duplicates' using either left or substring

select fileondisk, left(fileondisk,10)
from reporting.folder_list
where left(fileondisk,10) = 'Duplicates';

-- 2. Find the position of dot '.' in fileondisk

select position('.' in fileondisk), fileondisk
from reporting.folder_list;

-- 3. Extract text up but not including the dot '.' in fileondisk where there is a dot

-- 4. From the reporting.clientcontract table get contract that where completed in the following month
-- (this should take two queries but there aren't engagements in december that are completed in january)


-- 2. Find the position of dot '.' in fileondisk

select position('.' in fileondisk), fileondisk from reporting.folder_list;

-- 3. Extract text up but not including the dot '.' in fileondisk where there is a dot

select position('.' in fileondisk),
       fileondisk,
       substring(fileondisk,1,position('.' in fileondisk) - 1)
       from reporting.folder_list
where position('.' in fileondisk) > 0

-- 4. From the reporting.clientcontract table get contract that where completed in the following month
-- (this should take two queries but there aren't engagements in december that are completed in january)

select engagementdate
     , completiondate
     , completiondate - engagementdate as days
,date_part('month',engagementdate)
,date_part('month',completiondate)
,date_part('year',engagementdate)
,date_part('year',completiondate)
from reporting.clientcontract
where date_part('month',engagementdate) = date_part('month',completiondate) - 1
and date_part('year',engagementdate) = date_part('year',completiondate);
