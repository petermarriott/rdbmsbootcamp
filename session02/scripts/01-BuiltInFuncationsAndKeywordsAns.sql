
-- Original Practical 2

-- 1. Get all Items from OITM that start with Newage using either left or substring

select * 
 from OITM
where left(ItemName,6) = 'Newage'

-- 2. Extract kva value

select	ItemName, 
		substring(ItemName,
					charindex(' ',ItemName) + 1,
						charindex('kva', ItemName,
						charindex(' ',ItemName)) + 2 - charindex(' ',ItemName)),
		charindex('kva', ItemName,charindex(' ',ItemName) + 1)
 from OITM
where left(ItemName,6) = 'Newage'

-- 3. Change 3ph to 3PH in results

select	ItemName, 
		replace(ItemName, 'ph','PH')
 from OITM
where left(ItemName,6) = 'Newage'

-- 4. From the orders table get orders due in the next month

select * from ORDR
where DocDueDate between GETDATE() and dateadd(month,1,GETDATE())