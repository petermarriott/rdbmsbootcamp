
-- There are others see: Date and Time Functions (Transact-SQL) in help

-----------------------------------
-- Aggregate Functions and Group by
-----------------------------------

-- Whole table
select max(appointdate), min(appointdate), count(appointdate)
from reporting.appointment;

-- Group by clause
select clientid
     , max(appointdate)
     , min(appointdate)
     , count(appointdate)
from reporting.appointment
group by clientid
order by clientid;

-- Group by with having clause
select clientid
     , max(appointdate)
     , min(appointdate)
     , count(appointdate)
from reporting.appointment
group by clientid
having count(appointdate) > 5
order by clientid;

-- Group by clause using function values
select clientid
     , min(appointdate) as min_date
     , max(appointdate) as max_date
     , max(appointdate) - min(appointdate) as date_range_days
     , count(appointdate)
from reporting.appointment
group by clientid
having count(appointdate) > 5
order by clientid;

-- Group by year of birth
select date_part('year',dob) as year_of_birth
     , count(clientid)
from reporting.client
group by date_part('year',dob)
order by 1;

-- Group by age
select date_part('year',current_timestamp) - date_part('year',dob) as age
     , count(clientid)
from reporting.client
group by date_part('year',current_timestamp) - date_part('year',dob)
order by 1;

-- Group by age range
select ((date_part('year',current_timestamp) - date_part('year',dob))::int / 10) * 10 as group
     , count(clientid)
from reporting.client
group by ((date_part('year',current_timestamp) - date_part('year',dob))::int / 10) * 10
order by 1;

-- Practical

